package com.atguigu.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhupeng
 * http://localhost:3355/config/info 进行访问
 * 若配置中心文件修改，如果保证微服务不重启就可以获得最新的配置文件？？
 *
 * Config配置中心动态刷新--手动版
 * 手动获取刷新内容,避免微服务重启，问题是要一个一个通知微服务，比较麻烦！！！
 * 步骤：
 *  1. 修改完git配置文件后，请求刷新3355：curl -X POST http://localhost:3355/actuator/refresh
 *  2. @RefreshScope注解
 *
 *  解决方法：
 *  消息总线BUS-RabbitMQ解决
 *
 */
@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {
    @Value("${config.info}")
    private String configInfo;

    @GetMapping("/info")
    public String getConfigInfo() {
        return "3355通过3344配置中心，读取配置文件：" + configInfo;
    }
}
