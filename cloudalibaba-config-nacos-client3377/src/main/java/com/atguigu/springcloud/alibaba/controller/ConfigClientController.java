package com.atguigu.springcloud.alibaba.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhupeng
 * @RefreshScope 支持Nacos的动态刷新功能
 */
@RestController
@RefreshScope
public class ConfigClientController {
    @Value("${config.info}")
    private String configInfo;

    // 可以直接读取Nacos配置文件内容：nacos-config-client-dev.yaml
    @GetMapping("/config/info")
    public String getConfigInfo() {
        return configInfo;
    }
}
