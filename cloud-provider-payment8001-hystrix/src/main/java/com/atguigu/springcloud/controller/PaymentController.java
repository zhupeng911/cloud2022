package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (Payment)表控制层
 *
 * @date 2022/01/13 16:05
 */
@RestController
@RequestMapping("/payment")
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;
    @Value("${server.port}")
    private String serverPort;

    // 1. 模拟正常访问
    @GetMapping("/hystrix/ok/{id}")
    public String paymentInfoOK(@PathVariable("id") Integer id) {
        String result = paymentService.paymentInfoOK(id);
        return result;
    }

    // 2. 模拟服务降级：超时异常或者运行异常【一般在服务消费方进行服务降级】
    // 若大量线程访问timeout接口，线程池满，会导致其他接口ok也无法提供服务，即服务消费者80无法使用该接口
    @GetMapping("/hystrix/timeout/{id}")
    public String paymentInfoTimeOut(@PathVariable("id") Integer id) {
        String result = paymentService.paymentInfoTimeOut(id);
        return result;
    }

    // 3、服务熔断：【一般在服务提供方进行熔断】
    // 该接口达到大访问量后，直接拒绝服务，进行服务降级
    // 当监测到微服务响应正常后，恢复调用链路，默认5s内调用失败20次，会进行服务熔断
    @GetMapping("/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id) {
        String result = paymentService.paymentCircuitBreaker(id);
        return result;
    }
}
