package com.atguigu.springcloud.service;


import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class PaymentService {

    // 1. 模拟正常访问
    public String paymentInfoOK(Integer id) {
        String threadName = Thread.currentThread().getName();
        return "当前线程：" + threadName + " 调用方法：paymentInfoOK, 入参id:  " + id;
    }

    // 2. 模拟服务降级：超时异常或者运行异常【一般在服务消费方进行服务降级】
    @HystrixCommand(fallbackMethod = "paymentInfoTimeOutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000")})
    // 该接口最多处理2s，超过2s服务降级
    public String paymentInfoTimeOut(Integer id) {
        int sleepTime = 3;
        long start = System.currentTimeMillis();
        try {
            TimeUnit.SECONDS.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        String threadName = Thread.currentThread().getName();
        return "当前线程:  " + threadName + "调用方法：paymentInfoTimeOut,入参id:  " + id + "耗时(秒): " + (end - start);
    }

    // 2. 服务提供方服务降级：超时、异常
    public String paymentInfoTimeOutHandler(Integer id) {
        return "服务提供者-paymentInfoTimeOut-系统繁忙或者运行报错，请稍后再试...";
    }

    // 3、服务熔断：【一般在服务提供方进行熔断】
    // 10s内请求10次，如果请求失败率超过60%，进行服务熔断，具体熔断规则如下，可进行自定义配置
    // 当大量请求id为负数，即使id正数进行调用也无法调用成功，因为此时已经服务熔断，拒绝提供服务
    // 只有慢慢id为正数请求成功次数慢慢变多，才会恢复调用链路
    @HystrixCommand(fallbackMethod = "paymentCircuitBreakerFallback", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),                              // 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),                 // 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"),           // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),               // 失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(Integer id) {
        if (id < 0) {
            throw new RuntimeException("id不能负数");
        }
        String serialNumber = IdUtil.simpleUUID();
        return Thread.currentThread().getName() + "调用成功，流水号: " + serialNumber;
    }

    public String paymentCircuitBreakerFallback(Integer id) {
        return Thread.currentThread().getName() + "id 不能负数或超时或自身错误，请稍后再试，id： " + id;
    }
}
