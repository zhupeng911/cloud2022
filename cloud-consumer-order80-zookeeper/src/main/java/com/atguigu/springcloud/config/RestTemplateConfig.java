package com.atguigu.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhupeng
 * @Date 2022/4/6 7:33 PM
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    @LoadBalanced  // 使用@LoadBalanced注解赋予RestTemplate负载均衡的能力[轮流调用同一个服务名称的不同的接口]
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
