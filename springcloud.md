[TOC]

> spring boot 2.2.2 + spring cloud Hoxton.SR1 + spring cloud alibaba 2.1.0

# 1. SpringCloud微服务常用组件

![springcloud_001.png](img/springcloud_001.png)
![springcloud_002.png](img/springcloud_002.png)



# 2. Eureka-服务注册中心

```java
Eureka包含两个组件Eureka Server、Eureka Client
	Eureka Server: 提供服务发现的能⼒，各个微服务启动时，会通过Eureka Client向Eureka Server注册⾃⼰的信息（例如⽹络信息），Eureka Server会存储该服务的信息；
	Eureka Client: 是⼀个Java客户端，⽤于简化与Eureka Server的交互
    
[Eureka实操]
    cloud-eureka-server7001      [eureka注册中心]
    cloud-eureka-server7002      [eureka注册中心]
    cloud-provider-payment8001   [微服务提供者1] 在eureka进行注册
    cloud-provider-payment8002   [微服务提供者2] 在eureka进行注册，展示负载均衡效果
    cloud-consumer-order80       [微服务消费者] 在eureka进行注册
 
[代码实现]
    1、通过restTemplate调用其他微服务提供者的接口     PAYMENT_URL = "http://localhost:8001";
		2、通过euraka注册的微服务提供者名称调用           PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE" [xxx无法调用xxx!  设置fetchRegistry: true即可]
            restTemplate.postForObject(PAYMENT_URL + "/payment/create", payment, CommonResult.class);
            restTemplate.getForObject(PAYMENT_URL + "/payment/get/" + id, CommonResult.class) 
              
eureka集群[互相注册、相互守望]
	将payment8001、payment8002、order80注册进euraka集群中
	改/etc/hosts、改yml
```

>  很显然，restTemplate远程接口调用必须显示指明PAYMENT_URL，不优雅，需要优化



# 3. zookeeper-服务注册中心

> ​    cloud-provider-payment8004-zookeeper 	[zookeeper-微服务提供者]
> ​    cloud-consumer-order80-zookeeper     		[zookeeper-微服务消费者]
>
> ./zkServer.sh start
> ./zkServer.sh status
>
> zookeeper也可以作为一个服务注册中心





# 4. Ribbon-客户端负载均衡

> Ribbon客户端负载均衡 + restTemplate服务调用的工具，属于进程内负载均衡，往往集成在消费者方。

```java
spring-cloud-starter-netflix-eureka-client 默认自动引入了 spring-cloud-starter-netflix-ribbon
cloud-consumer-order80      [微服务消费者,更新]
  
1. 自定义负载均衡策略
@Configuration
public class SelfRibbonRule {
    /**
     * new RandomRule();            // 随机
     * new RoundRobinRule();	    	// 轮训
     * new RetryRule();             // 规定时间内重试
     * new WeightedResponseTimeRule();      // 响应速度越快的实例权重越大，越容易被选中
     * new BestAvailableRule();             // 过滤掉经常出问题的服务，选择并发量最小的
     * new AvailabilityFilteringRule();     // 并发最小的
     * new ZoneAvoidanceRule()
     * @return
     */
    @Bean
    public IRule iRule() {
        return new RandomRule(); // 轮训改为随机负载均衡
    }
}

2. 开启负载均衡
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration = SelfRibbonRule.class) // 启动该服务时去加载自定义的ribbon配置
public class CloudConsumerOrder80Application {
    public static void main(String[] args) {
        SpringApplication.run(CloudConsumerOrder80Application.class, args);
        System.out.println("启动成功");
    }
}

3. 使用
		// public static final String PAYMENT_URL = "http://localhost:8001";
    public static final String PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentForObject(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PAYMENT_URL + "/payment/get/" + id, CommonResult.class);
    }
```



# 5. Feign+OpenFeign-客户端负载均衡 + 服务调用

> Feign、OpenFeign = Ribbon + restTemplate 客户端负载均衡 + 服务调用 [在service写controller！！] cloud-consumer-openfeign-order80    [openFeign服务调用]

使用：

```java
/**
 * @date 2020-02-19 23:59
 * 相当于把要调用微服务的controller代码在这声明一下！！！
 * 通过注解@FeignClient(value = "CLOUD-PAYMENT-SERVICE")调用其他微服务的接口[代替restTemplate硬编码调用],以声明式方法实现接口到接口的调用
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {
    @GetMapping(value = "/payment/get/{id}")
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/queryAllByLimit")
    CommonResult<List<Payment>> queryAllByLimit(@RequestParam(value = "offset") int offset, @RequestParam(value = "limit") int limit);

    @GetMapping(value = "/lb")
    String getPaymentLB();

    @GetMapping(value = "/payment/feign/timeout")
    String paymentFeignTimeout();
}
```



# 6. Hystrix-断路器

> Hystrix可以进行服务降级[超时、异常、服务熔断、线程池满]、服务熔断、服务限流[秒杀高并发场景]、接近实时的监控，一般在服务消费方进行服务降级、服务提供方进行服务熔断

```java
1. cloud-eureka-server7001                     [eureka注册中心]
2. cloud-provider-payment8001-hystrix          [服务提供者]
    // 2. 模拟服务降级：超时异常或者运行异常【一般在服务消费方进行服务降级】
    @HystrixCommand(fallbackMethod = "paymentInfoTimeOutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000")})
    // 该接口最多处理2s，超过2s服务降级
    public String paymentInfoTimeOut(Integer id) {
  			TimeUnit.SECONDS.sleep(3);
    }

    // 2. 服务提供方服务降级：超时、异常
    public String paymentInfoTimeOutHandler(Integer id) {
        return "服务提供者-paymentInfoTimeOut-系统繁忙或者运行报错，请稍后再试...";
    }

    // 3、服务熔断：【一般在服务提供方进行熔断】
    // 10s内请求10次，如果请求失败率超过60%，进行服务熔断，具体熔断规则如下，可进行自定义配置
    // 当大量请求id为负数，即使id正数进行调用也无法调用成功，因为此时已经服务熔断，拒绝提供服务
    // 只有慢慢id为正数请求成功次数慢慢变多，才会恢复调用链路
    @HystrixCommand(fallbackMethod = "paymentCircuitBreakerFallback", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),                              // 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),                 // 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"),           // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),               // 失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(Integer id) {
        if (id < 0) {
            throw new RuntimeException("id不能负数");
        }
        String serialNumber = IdUtil.simpleUUID();
        return Thread.currentThread().getName() + "调用成功，流水号: " + serialNumber;
    }

    public String paymentCircuitBreakerFallback(Integer id) {
        return Thread.currentThread().getName() + "id 不能负数或超时或自身错误，请稍后再试，id： " + id;
    }
```

```java
3. cloud-consumer-openfeign-hystrix-order80    [服务消费者 新增容错机制]
服务提供方降级：
@Component
public class PaymentFallbackServiceImpl implements PaymentHystrixService {
    @Override
    public String paymentInfoOK(Integer id) {
        return "微服务提供者down机-服务消费者-paymentInfoOK-服务降级方法";
    }
}
  
服务消费方降级
 		// 2. 模拟服务降级：超时异常或者运行异常
    // 消费者服务降级，如果自己只能等1.5s，否则消费者进行服务降级
    // 问题：一一对应降级方法导致耦合度高、代码膨胀
    // 解决方法：全局服务降级
    @GetMapping("/payment/hystrix/timeout/{id}")
    @HystrixCommand(fallbackMethod = "paymentTimeOutFallbackMethod", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000")
    })
    public String paymentInfoTimeOut(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentInfoTimeOut(id);
        return result;
    }

    public String paymentTimeOutFallbackMethod(@PathVariable("id") Integer id) {
        return "我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己";
    }

    // 3. 服务降级：解耦合、全局服务降级
    @GetMapping("/payment/hystrix/timeout/{id}")
    @HystrixCommand
    public String paymentInfoTimeOut(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentInfoTimeOut(id);
        return result;
    }

    // 默认全局服务降级，接口如果有自定义的服务降级就要自己的，如果没有则用户全局的
    public String paymentGlobalFallbackMethod() {
        return "payment_Global_FallbackMethod异常处理信息，请稍后再试";
    }
```



# 7. Zuul+GateWay-服务网关

> 功能：gateway之所以性能好,因为底层使用WebFlux,而webFlux底层使用netty通信(NIO),动态路由可以对路由指定断言Predicate和过滤Filter,集成了Hystrix的断路器进行服务降级、服务熔断、服务限流

```yaml
spring:
  application:
    name: cloud-gateway
  cloud:
    gateway:
      discovery:
        locator:
          enabled: true   # 开启从注册中心动态创建路由的功能，利用微服务名进行路由
      routes:
        # 1.将本地http://localhost:8001外加一层网关9527
        #        - id: payment_routh                   # 路由ID: 没有固定规则但要求唯一，建议配合服务名
        #          uri: http://localhost:8001          # 匹配后提供服务的路由地址
        #          predicates:
        #            - Path=/payment/get/**            # 断言:路径相匹配的进行路由
        #
        #        - id: payment_routh2
        #          uri: http://localhost:8001          # 匹配后提供服务的路由地址
        #          predicates:
        #            - Path=/payment/lb/**              # 断言，路径相匹配的进行路由

        # 2.开启从注册中心动态创建路由的功能，利用微服务名进行路由
        - id: payment_routh
          uri: lb://cloud-payment-service
          predicates:
            - Path=/payment/get/**
        - id: payment_routh2
          uri: lb://cloud-payment-service
          predicates:
            - Path=/payment/lb/**
            - After=2022-05-10T10:59:34.102+08:00[Asia/Shanghai]
#            - Before=2020-03-08T10:59:34.102+08:00[Asia/Shanghai]
#            - Between=2020-03-08T10:59:34.102+08:00[Asia/Shanghai] ,  2020-03-08T10:59:34.102+08:00[Asia/Shanghai]
#            - Cookie=username,zzyy   # Cookie=cookieName,正则表达式
#            - Header=X-Request-Id, \d+
#            - Host=**.atguigu.com  # curl http://localhost:9527/payment/lb -H "Host:afae.atguigu.com"
```

> 实现将路径访问 http://localhost:8001/payment/get/1 改为 http://localhost:9527/payment/get/1
>



# 8. Config-服务配置 

> 由config-server、congfig-client两部分组成，为各个不同的微服务应用的所有环境提供了一个中心化的外部配置中心，可以是本机、github等
>
> ​    cloud-eureka-server7001                     [eureka注册中心]
> ​    cloud-config-center-3344                    [配置中心服务器]
> ​    cloud-config-center-3355                    [配置中心客户端]

```yaml
spring:
  application:
    name: cloud-config-center
  cloud:
    config:
      server:
        git:
          uri: https://gitee.com/zhupeng911/sprincloud-config.git   # GitHub上面的git仓库名字
          search-paths:
            - springcloud-config  # 搜索目录
          force-pull: true
          username: zhupeng911
          password: zhupeng8023zcx,,
      label: master               # 读取分支
```



# 9. Stream-消息驱动

> 通过Stream来操作常见的消息中间件RabbitMQ、RocketMQ、ActiveMQ、Kafak，屏蔽底层消息中间件的差异

```java
Binder：		绑定器，是应用与消息中间件之间的封装，很方便的连接中间件，屏蔽差异
Channel： 	通道，是队列Queue的一种抽象，在消息通讯系统中就是实现存储哥转发的媒介，通过Channel对队列进行配置
Source/Sink：	简单的可理解为参照对象是SpringCloud-Stream本身，从Stream发布消息就是输出，接受消息就是输
```

    @Input：输入通道，通过该通道接收到的消息进入应用程序
    @Output：输出通道，发布的消息将通过通道离开应用程序
    @StreamListener：监听队列，用于消费者队列的消息接受
    @EnableBinding: 指信道Channel与exChange绑定在一起
    
    cloud-eureka-server7001                     [eureka注册中心]
    cloud-stream-rabbitmq-provider8801          [rabbitmq消息驱动-生产者]
    cloud-stream-rabbitmq-consumer8802          [rabbitmq消息驱动-消费者]
    cloud-stream-rabbitmq-consumer8803          [rabbitmq消息驱动-消费者-避免重复消费、消息持久化]



