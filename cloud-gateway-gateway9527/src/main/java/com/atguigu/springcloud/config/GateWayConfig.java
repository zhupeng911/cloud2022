package com.atguigu.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhupeng
 * <p>
 * 通过硬编码配置路由
 * http://localhost:9527/baidu 路由到 https://www.baidu.com
 */
@Configuration
public class GateWayConfig {
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("route_baidu",
                        r -> r.path("/baidu").uri("https://www.baidu.com"))
                .build();
        return routes.build();
    }
}

