package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.service.PaymentHystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhupeng
 */
@RestController
@RequestMapping("/consumer")
@Slf4j
@DefaultProperties(defaultFallback = "paymentGlobalFallbackMethod")
public class OrderHystrixController {
    @Resource
    private PaymentHystrixService paymentHystrixService;

    // 1. 模拟正常访问，调用8001微服务提供的接口
    @GetMapping("/payment/hystrix/ok/{id}")
    public String paymentInfoOK(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentInfoOK(id);
        return result;
    }

    // 2. 模拟服务降级：超时异常或者运行异常
    // 消费者服务降级，如果自己只能等1.5s，否则消费者进行服务降级
    // 问题：一一对应降级方法导致耦合度高、代码膨胀
    // 解决方法：全局服务降级
//    @GetMapping("/payment/hystrix/timeout/{id}")
//    @HystrixCommand(fallbackMethod = "paymentTimeOutFallbackMethod", commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000")
//    })
//    public String paymentInfoTimeOut(@PathVariable("id") Integer id) {
//        String result = paymentHystrixService.paymentInfoTimeOut(id);
//        return result;
//    }
//
//    public String paymentTimeOutFallbackMethod(@PathVariable("id") Integer id) {
//        return "我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己";
//    }

    // 3. 服务降级：解耦合、全局服务降级
    @GetMapping("/payment/hystrix/timeout/{id}")
    @HystrixCommand
    public String paymentInfoTimeOut(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentInfoTimeOut(id);
        return result;
    }

    // 默认全局服务降级，接口如果有自定义的服务降级就要自己的，如果没有则用户全局的
    public String paymentGlobalFallbackMethod() {
        return "payment_Global_FallbackMethod异常处理信息，请稍后再试";
    }
}
