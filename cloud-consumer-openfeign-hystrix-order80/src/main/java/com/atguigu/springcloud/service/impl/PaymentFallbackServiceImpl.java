package com.atguigu.springcloud.service.impl;

import com.atguigu.springcloud.service.PaymentHystrixService;
import org.springframework.stereotype.Component;

/**
 * 对PaymentHystrixService统一进行服务降级接口，并且一一对应
 */
@Component
public class PaymentFallbackServiceImpl implements PaymentHystrixService {

    @Override
    public String paymentInfoOK(Integer id) {
        return "微服务提供者down机-服务消费者-paymentInfoOK-服务降级方法";
    }

    @Override
    public String paymentInfoTimeOut(Integer id) {
        return "微服务提供者down机-服务消费者-paymentInfoTimeOut-服务降级方法";
    }
}
