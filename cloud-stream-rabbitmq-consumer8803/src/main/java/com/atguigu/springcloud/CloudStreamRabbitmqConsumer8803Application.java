package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhupeng
 * @Date 2022/5/1 3:06 PM
 */
@SpringBootApplication
public class CloudStreamRabbitmqConsumer8803Application {
    public static void main(String[] args) {
        SpringApplication.run(CloudStreamRabbitmqConsumer8803Application.class, args);
        System.out.println("启动成功");
    }
}
