package com.atguigu.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * 8802 接收消息
 *
 * @author lixiaolong
 * @date 2020/12/31 14:07
 */
@Component
@EnableBinding(Sink.class)
public class ReceiveMessageListener {
    @Value("${server.port}")
    private String serverPort;

    // 消费者监听RabbitMQ消息中间件的消息
    @StreamListener(Sink.INPUT)
    public void input(Message<String> message) {
        System.out.println("消费者:" + serverPort + "接受到的消息：" + message.getPayload());
    }
}
