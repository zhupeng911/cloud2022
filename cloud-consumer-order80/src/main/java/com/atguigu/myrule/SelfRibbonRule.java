package com.atguigu.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @date 2020/12/22 20:54
 * @description 该类是ribbon的自定义策略
 */
@Configuration
public class SelfRibbonRule {

    /**
     * new RandomRule();            // 随机
     * new RoundRobinRule();	    // 轮训
     * new RetryRule();             //规定时间内重试
     * new WeightedResponseTimeRule();      // 响应速度越快的实例权重越大，越容易被选中
     * new BestAvailableRule();             // 过滤掉经常出问题的服务，选择并发量最小的
     * new AvailabilityFilteringRule();     //并发最小的
     * new ZoneAvoidanceRule()
     *
     * @return
     */
    @Bean
    public IRule iRule() {
        return new RandomRule(); // 轮训改为随机负载均衡
    }
}
