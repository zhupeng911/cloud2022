[TOC]

# 1.  简介

> Spring Cloud Netflix项目进入维护模式，也就是不更新了,不会开发新组件了，所以某些组件都有代替版了比如:
>
> 1. Eureka停用，zookeeper作为服务注册中心 
> 2. Ribbon准备停更,代替为LoadBalancer
> 3. Feign改为OpenFeign 
> 4. Hystrix停更,改为resilence4j或者阿里巴巴的sentienl
> 5. Zuul改为gateway 
> 6. 服务配置Config改为 Nacos
> 7. 服务总线Bus改为Nacos

![springcloud_002.png](/Users/zhupeng/朱鹏/javaDoc/images/springcloud_002.png)

```java
核心组件:
Nacos: 				Nacos = erueka注册中心 + config配置中心 + Bus消息总线
sentinel:     面向分布式服务架构的流量控制组件，主要以流量为切入点，从限流、流量整形、熔断降级、系统负载保护、热点防护等多个维度来帮助开发者保障微服务的稳定性。
seata:        处理分布式事务  
  
官网链接
    [Nacos]     https://nacos.io/zh-cn/  
    [Sentinel]  https://github.com/alibaba/Sentinel/wiki/
    [seata]     http://seata.io/zh-cn/index.html
```



# 2. Nacos

## 2.1 安装部署

```java
Nacos安装部署：参考 https://blog.csdn.net/w_317/article/details/110260118
    1、下载：https://nacos.io/zh-cn/、https://github.com/alibaba/nacos
    2、启动：
        cd /Users/zhupeng/nacos/bin
        sh startup.sh -m standalone
    3、访问 
        localhost:8848/nacos  账号密码:默认都是nacos、默认监听8848
```



## 2.2 Nacos之注册中心

> cloudalibaba-provider-payment9001 		[微服务提供者-注册进Nacos]
> cloudalibaba-provider-payment9002		 [微服务提供者-注册进Nacos],用于模拟负载均衡
> cloudalibaba-consumer-nacos-order83 	[微服务消费者-注册进Nacos]

    http://localhost:9001/payment/nacos/1
    http://localhost:9002/payment/nacos/1
    http://localhost:83/consumer/payment/nacos/1 [若微服务提供者集群提供2个接口可以访问，则自动实现负载均衡]



- **Nacos支持CP、AP切换，默认AP模式**

  C[Consistency]：强一致性，在分布式系统中，如果服务器集群，每个节点在同时刻访问必须要保持数据的一致性

  A[Availability]：高可用性，集群节点中，部分节点出现故障后任然可以使用 （高可用）

  P[Partition tolerance]：分区容错性，在分布式系统中网络会存在脑裂的问题，部分Server与整个集群失去节点联系，无法组成一个群体。

  > 切换方式：$NACOS_SERVER:8848/nacos/v1/ns/operator/switches?entry=serverMode&value=CP



## 2.3 Nacos之配置中心

> 代替Config在github保存的配置文件信息，文件名称固定规则。

```java
cloudalibaba-config-nacos-client3377 [Nacos之配置中心]

配置Nacos文件名称规则   
    ${spring.application.name}-${spring.profile.active}.${spring.cloud.nacos.config.file-extension} 
    eg：nacos-config-client-dev.yaml   ----> 就可以读取到Nacos的该配置文件内容：config.info
http://localhost:3377/config/info 访问即可
```



- **高级配置**
      Namespace：区分部署环境public/dev/test，默认public，不同Namespace是隔离的
      Group：		  DEFAULT_GROUP，默认集群是DEFAULT，可以将不同的微服务划分到同一个分组
      DataID：		 指定不同的环境，比如：dev、test、prod，也就是spring.profiles.active=dev

![img.png](img/img.png)

> 结论：Nacos可以通过Namespace、Group、DataID去读取满足不同条件的配置文件，很明显比SpringCloud提供的Config在Github读取固定的配置文件功能强大



## 2.4 Nacos之持久化与集群

> 参考官网：https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html

- **Nacos持久化：切换数据库为mysql，默认derby，实现配置信息、服务信息持久化**

```java
1. 改配置文件application.properties，增加以下数据库内容    
  spring.datasource.platform=mysql
  db.num=1
  db.url.0=jdbc:mysql://127.0.0.1:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
  db.user=root
  db.password=zhupeng123
    
2. 执行nacos-mysql.sql，生成相关数据库表
    
3. 重启Nacos
  cd /Users/zhupeng/nacos/bin
  ./shutdown.sh
  sh startup.sh -m standalone
    
4. 访问：localhost:8848/nacos，重新配置成功
```


![img_1.png](img/img_1.png)



- **Nacos之集群**

```yaml
1、完成上述数据库持久层
2、集群配置文件cluster.conf
        192.168.1.104:3333
        192.168.1.104:4444
        192.168.1.104:5555
3、编辑startup.sh[不需要配置了]
4、启动3个Nacos
        ./startup.sh -p 3333
        ./startup.sh -p 4444
        ./startup.sh -p 5555
        
⇒  ps -ef | grep nacos | grep -v grep | wc -l    // 3
```







# 3. Sentinel

> 官网：https://github.com/alibaba/Sentinel/wiki/ 
>
> 取代Hystrix，实现服务降级、服务熔断、服务限流、热点参数限流、系统自适应保护等功能

## 3.1 安装部署

```java
Sentinel安装部署
    1、下载：https://github.com/alibaba/Sentinel/releases/tag/1.8.4
    2、启动：
        java -jar sentinel-dashboard-1.8.4.jar // 8080端口不能被占有
    3、访问
        http://localhost:8080/#/login           账号密码:默认都是sentinel
        PS: 必须先调用一下接口，再访问Sentinel
          
    cloudalibaba-sentinel-service8401 [微服务，注册进Nacos8848，sentinel8080保护]
```



## 3.2 Sentinel之流量控制

- **流控规则**

```java
资源名：		 请求接口URL路径，唯一名称
针对来源：		可以针对调用者进行限流，填写微服务名称，默认default(不区分来源)
域值类型：
		QPS：	每秒钟的请求数量，当超过该数量则进行限流
		线程数：当调用该api的线程数达到该域值，则进行限流
流控模式：
		直接[默认]：api达到限流条件，直接限流
		关联：			当关联的资源达到域值，就限流自己，B惹事，A挂了
		链路：			只记录指定链路的流量[api级别的针对来源] //todo
流控效果：
		快速失败：			直接失败，抛出异常信息[Blocked by Sentinel (flow limiting)]
		Warmup预热: 	单机域值/coldFactor[默认是3]，经过预热时长后才会达到域值
		排队等待：			匀速排队，让请求以均匀的速度通过，域值类型必须是QPS
```

<img src="/Users/zhupeng/朱鹏/javaDoc/images/img_2.png" alt="img.png" style="zoom:67%;" />



例1：域值类型：QPS、线程数[略]

例2：流控模式 = 关联
    若/testB调用的QPS>1，则/testA限流不允许访问
    ![img.png](img/img_3.png)



例3： 流控效果 = Warmup预热
    希望/testA最终域值为10，但是从域值为10/3=3开始，经过10s达到预值为10，若是在第1s访问量超过3则报错，10s之后访问量大于10则报错
![img.png](img/img_4.png)

例4： 流控效果 = 排队等待
    希望/testB以每秒1次请求处理，超过的话则排队等待，等待的超时时间为2000毫秒    ![img.png](img/img_5.png)



## 3.3 Sentinel之服务降级

> 1. 除了流量控制以外，对调用链路中不稳定的资源进行熔断降级也是保障高可用的重要措施之一。一个服务常常会调用别的模块，可能是另外的一个远程服务、数据库，或者第三方 API 等。
> 2. 如果依赖的服务出现了不稳定的情况，请求的响应时间变长，那么调用服务的方法的响应时间也会变长，线程会产生堆积，最终可能耗尽业务自身的线程池，服务本身也变得不可用。

- **降级规则**

![img.png](/Users/zhupeng/朱鹏/javaDoc/images/img_6.png)

```java
熔断策略：
  1.慢调用比例 (SLOW_REQUEST_RATIO)：
      最大RT：即最大的响应时间，请求的响应时间大于该值则统计为慢调用。
      比例阈值：慢调用的比例
      熔断时长: 故名思议，就是服务熔断，不能提供的时间
      统一时长：默认1s
      最小请求书：默认5
	当统计时长内，接口请求数目大于设置的最小请求数目，并且慢调用的比例大于比例阈值，则接下来的熔断时长内请求会自动被熔断

	2.异常比例 (ERROR_RATIO)：
			比例阈值：慢调用的比例
	当单位统计时长内请求数目大于设置的最小请求数目，并且异常的比例大于比例阈值，则接下来的熔断时长内请求会自动被熔断
        
	3. 异常数 (ERROR_COUNT)：
	当单位统计时长内的异常数目超过阈值之后会自动进行熔断。若接下来的一个请求成功完成（没有错误）则结束熔断，否则会再次被熔断。
```



例1：熔断策略=慢调用比例RT
    ![img.png](img/img_7.png)

> 当1s内，请求接口/testD的数目大于5个，并且慢调用(接口处理时间大于200s)的比例大于100%，则接下来的1s会自动被熔断



## 3.4 Sentinel之热点key限流

> 很多时候我们希望统计某个热点数据中访问频次最高的 Top K 数据，并对其访问进行限制。

![img.png](img/img_8.png)



例1： 对接口/testHotKey的第一个参数p1进行热点限流，在1s内限制1次访问，若超过域值，进行报错处理
    ![img.png](img/img_9.png)



例2：对接口/testHotKey的第一个参数p1进行热点限流，在1s内限制1次访问，若超过域值，进行自定义报错处理，**但是p1=1特殊值的时，域值为100，超过再限流 [高级选项：参数例外项]**
    ![img.png](img/img_10.png)



## 3.5 Sentinel之系统规则
> Sentinel系统自适应限流从整体维度对应用入口流量进行控制，结合应用的 Load、CPU 使用率、总体平均 RT、入口 QPS 和并发线程数等几个维度的监控指标，通过自适应的流控策略，让系统的入口流量和系统的负载达到一个平衡，让系统尽可能跑在最大吞吐量的同时保证系统整体的稳定性。

![img.png](img/img_11.png)

```java
系统规则支持以下的模式：
    Load 自适应：   仅对 Linux/Unix-like 机器生效, 当系统 load1 超过设定的启发值，且系统当前的并发线程数超过估算的系统容量时才会触发系统保护（BBR 阶段），设定参考值一般是 CPU cores * 2.5。
    RT：           当单台机器上所有入口流量的平均 RT 达到阈值即触发系统保护，单位是毫秒。
    并发线程数：     当单台机器上所有入口流量的并发线程数达到阈值即触发系统保护。
    入口QPS：       当单台机器上所有入口流量的 QPS 达到阈值即触发系统保护。
    CPU 使用率：    当系统 CPU 使用率超过阈值即触发系统保护（取值范围 0.0-1.0），比较灵敏。

// 服务降级的默认兜底方法，并解耦合！！！！，与Hystrix一一对应不同，这里是选择自定义的兜底方法
    @GetMapping("/rateLimit/customerBlockHandler")
    @SentinelResource(value = "customerBlockHandler",
        blockHandlerClass = CustomerBlockHandler.class,
        blockHandler = "handlerException2")
    public CommonResult customerBlockHandler() {
        return new CommonResult(200, "按客戶自定义", new Payment(2020L, "serial003"));
    }

实现效果：替代Hystrix
    1、用户可以自定义兜底方法，实现服务降级
    2、自定义的兜底方法与业务方法解耦合
    3、实现全局统一的处理方法
```





## 3.6 Sentinel之服务熔断]

```java
cloudalibaba-provider-payment9001
cloudalibaba-provider-payment9002
cloudalibaba-consumer-nacos-order84     [整合Feign、调用微服务9003、9004，之前是restTemplate]
```



## 3.7 Sentinel之规则持久化

```yml
步骤：
    1、Sentinel8401 导入依赖
        <dependency>
        <groupId>com.alibaba.csp</groupId>
        <artifactId>sentinel-datasource-nacos</artifactId>
        </dependency>
    2、Sentinel8400 yml增加配置
        datasource:
        ds1:
        nacos:
        server-addr: localhost:8848     # sentinel配置的内容持久化到nacos8848
        dataId: ${spring.application.name}
        groupId: DEFAULT_GROUP
        data-type: json
        rule-type: flow
		 3、在Nacos手写Sentinel的配置信息，将自定同步到Sentinel
        [
            {
                "resource": "/testA",
                "limitApp": "default",
                "grade": 1,
                "count": 1,
                "strategy": 0,
                "controlBehavior": 0,
                "clusterMode": false
            }
        ]       
```







# 4. seata

> 解决全局的数据一致性：一次业务逻辑如果需要跨多个数据源或者跨多个系统操作不同的数据库，就必然会产生分布式事务的问题，也就是每个服务内部的一致性由本地事务来保证，但是全局的数据一致性没法保证

## 4.1 seata安装部署

```
  	1、下载：https://github.com/seata/seata/releases
    2、修改配置文件： conf/file.conf
        事务日志存储模式修改为db
        数据库连接信息[注意数据源的选择]
    3、建数据库、表    https://github.com/seata/seata/blob/develop/script/server/db/mysql.sql
    4、修改registry配置文件
        注册进Nacos
    5、启动
        先启动Nacos
        再启动seata     ./seata-server.sh
```

## 4.2 核心内容

```
核心内容：1 ID + 3 组件模型
    TransactionID XID ：全局的事务ID
    TC (Transaction Coordinator) - 事务协调者
        维护全局和分支事务的状态，驱动全局事务提交或回滚。
    TM (Transaction Manager) - 事务管理器
        定义全局事务的范围：开始全局事务、提交或回滚全局事务。 
    RM (Resource Manager) - 资源管理器
        管理分支事务处理的资源，与TC交谈以注册分支事务和报告分支事务的状态，并驱动分支事务提交或回滚。
```

- **处理流程**
  ![img.png](img/img_12.png)

```java
		1、TM向TC申请开启一个全局事务，全局事务创建成功并生成一个全局唯一的XID
    2、XID在微服务调用链路的上下文传播
    3、RM向TC注册分支事务，将其纳入XID对应全局事务的管辖
    4、TM向TC发起XID的全局提交活回滚协议
    5、TC调度XID下管辖的全部分支事务完成提交或回滚请求
```

## 4.3 案例分析

```
需求：
    下订单、减库存、扣余额、改订单状态
    seata-order-service2001
    seata-account-service2002
    seata-storage-service2003
```
