package com.atguigu.springcloud.service;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @date 2020-02-19 23:59
 * 相当于把要调用微服务的controller代码在这声明一下！！！
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {
    @GetMapping(value = "/payment/get/{id}")
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/queryAllByLimit")
    CommonResult<List<Payment>> queryAllByLimit(@RequestParam(value = "offset") int offset, @RequestParam(value = "limit") int limit);

    @GetMapping(value = "/lb")
    String getPaymentLB();

    @GetMapping(value = "/payment/feign/timeout")
    String paymentFeignTimeout();
}
