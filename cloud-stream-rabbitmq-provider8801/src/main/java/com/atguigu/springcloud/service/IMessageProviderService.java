package com.atguigu.springcloud.service;

/**
 * @author zhupeng
 * @Date 2022/5/1 2:34 PM
 */
public interface IMessageProviderService {
    // 定义消息的推送管道
    String send();
}
