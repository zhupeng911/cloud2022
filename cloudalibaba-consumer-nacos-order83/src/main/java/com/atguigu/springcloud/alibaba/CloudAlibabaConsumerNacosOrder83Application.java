package com.atguigu.springcloud.alibaba;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zhupeng
 * @EnableDiscoveryClient 该注解用于向使用consul或者zookeeper作为注册中心时注册服务
 */
@EnableDiscoveryClient
@SpringBootApplication
public class CloudAlibabaConsumerNacosOrder83Application {
    public static void main(String[] args) {
        SpringApplication.run(CloudAlibabaConsumerNacosOrder83Application.class, args);
        System.out.println("启动成功");
    }
}
