package com.atguigu.springcloud.aliba.service;

import com.atguigu.springcloud.aliba.service.impl.PaymentFallbackServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author zhupeng
 * @date 2020-02-25 18:15
 */
@FeignClient(value = "nacos-payment-provider", fallback = PaymentFallbackServiceImpl.class)
public interface PaymentService {
    @GetMapping(value = "/payment/nacos/{id}")
    String fetchPayment(@PathVariable("id") Long id);
}
