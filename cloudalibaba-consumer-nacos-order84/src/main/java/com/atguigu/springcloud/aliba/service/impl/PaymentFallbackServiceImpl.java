package com.atguigu.springcloud.aliba.service.impl;

import com.atguigu.springcloud.aliba.service.PaymentService;
import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import org.springframework.stereotype.Component;

/**
 * @author zhupeng
 * @date 2020-02-25 18:30
 */
@Component
public class PaymentFallbackServiceImpl implements PaymentService {
    @Override
    public String fetchPayment(Long id) {
        Payment payment = new Payment(id, "errorSerial");
        return new CommonResult<Payment>(44444, "服务降级返回,---PaymentFallbackService", payment).getMessage();
    }
}
