package com.atguigu.springcloud.aliba;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
public class CloudAlibabaConsumerNacosOrder84Application {
    public static void main(String[] args) {
        SpringApplication.run(CloudAlibabaConsumerNacosOrder84Application.class, args);
        System.out.println("启动成功");
    }
}
